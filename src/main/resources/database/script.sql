create table dishes(
                       id serial NOT NULL,
                       name varchar(255) NOT NULL,
                       description text,
                       price double precision NOT NULL,
                       is_available boolean,
                       PRIMARY KEY (id)
);

create type office as enum (
    'Undefined',
    'GeneralManager',
    'Chef',
    'KitchenHelper',
    'Washer',
    'RestaurantManager',
    'Waiter',
    'BarManager',
    'Barman',
    'Runner');


create table storage(
                        id serial not null,
                        name varchar(255),
                        quantity double precision,
                        primary key (id)
);

create table drinks(
                       id serial not null ,
                       name varchar(255) not null,
                       description text,
                       price double precision not null,
                       portion integer,
                       is_available boolean not null,
                       primary key (id)
);

create table employee(
                         id serial not null,
                         name varchar(255) not null ,
                         surname varchar(255) not null ,
                         pesel bigint not null ,
                         position office default 'Undefined'::office,
                         head BIGINT,
                         primary key (id),
                         foreign key (head) references employee(id)
);

create table client(
                       id serial not null ,
                       name varchar(255) not null ,
                       surname varchar(255) not null ,
                       discount integer,
                       primary key (id)
);

create table bill(
                     id serial not null,
                     full_price double precision not null,
                     tip double precision,
                     id_employee bigint not null ,
                     id_client bigint not null ,
                     data date not null,
                     primary key (id),
                     foreign key (id_employee) references employee(id),
                     foreign key (id_client) references client(id)
);

create table dishes_bill(
                            id serial not null ,
                            id_dish bigint not null,
                            id_bill bigint not null,
                            primary key (id),
                            foreign key (id_dish) references dishes(id),
                            foreign key (id_bill) references bill(id)
);

create table drink_bill(
                           id serial not null ,
                           id_drink bigint not null ,
                           id_bill bigint not null ,
                           primary key (id),
                           foreign key (id_bill) references bill(id),
                           foreign key (id_drink) references drinks(id)
);

create table drink_storage(
                              id serial not null ,
                              id_drink bigint not null ,
                              id_storage bigint not null,
                              primary key (id),
                              foreign key (id_drink) references drinks(id),
                              foreign key (id_storage) references storage(id)
);

create table dish_storage(
                             id serial not null ,
                             id_dish bigint not null,
                             id_storage bigint not null,
                             primary key (id),
                             foreign key (id_dish) references dishes(id),
                             foreign key (id_storage) references storage(id)
);
