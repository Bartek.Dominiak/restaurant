INSERT INTO dishes(id, name, description, price, is_available) VALUES (1, 'small kebab', 'small kebab with chicken and mixed sauce', 5.99, true);
INSERT INTO dishes(id, name, description, price, is_available) VALUES (2, 'medium kebab', 'medium kebab with chicken and mixed sauce', 6.99, true);
INSERT INTO dishes(id, name, description, price, is_available) VALUES (3, 'big kebab', 'big kebab with chicken and mixed sauce', 7.99, false);
INSERT INTO dishes(id, name, description, price, is_available) VALUES (4, 'giant kebab', 'giant kebab with chicken and mixed sauce', 7.99, true);

INSERT INTO storage(id, name, quantity) values (1, 'small kebab', 10);
INSERT INTO storage(id, name, quantity) values (2, 'medium kebab', 3);
INSERT INTO storage(id, name, quantity) values (3, 'big kebab', 0);
INSERT INTO storage(id, name, quantity) values (4, 'giant kebab', 20);
INSERT INTO storage(id, name, quantity) values (5, 'big beer', 20);
INSERT INTO storage(id, name, quantity) values (6, 'bigger beer', 0);
INSERT INTO storage(id, name, quantity) values (7, 'the biggest beer', 1);

INSERT INTO drinks(id, name, description, price, portion, is_available) VALUES (1, 'big beer', '500 ml of big beer', 0.99, 500, true);
INSERT INTO drinks(id, name, description, price, portion, is_available) VALUES (2, 'bigger beer', '1000 ml of bigger beer', 1.01, 1000, false);
INSERT INTO drinks(id, name, description, price, portion, is_available) VALUES (3, 'the biggest beer', '5000 ml of the biggest beer', 1.10, 1000, true);

INSERT INTO employee(id, name, surname, pesel, position,head_id) VALUES (1, 'Janusz', 'Kowalski', 123, 'GENERAL_MANAGER', null);
INSERT INTO employee(id, name, surname, pesel, position, head_id) VALUES (2, 'Patrycja', 'Wiśniewska', 1223, 'BAR_HEAD', 1);
INSERT INTO employee(id, name, surname, pesel, position, head_id) VALUES (3, 'Jan', 'Kowalski', 23, 'BARMAN',2);
INSERT INTO employee(id, name, surname, pesel, position) VALUES (4, 'Jan', 'Nowak', 2333, 'RUNNER');
INSERT INTO employee(id, name, surname, pesel, position) VALUES (5, 'Jan', 'Nowakowski', 20333, 'RUNNER');
INSERT INTO employee(id, name, surname, pesel, position) VALUES (6, 'Jan', 'Nowaczek', 21333, 'CHEF');
INSERT INTO employee(id, name, surname, pesel, position) VALUES (7, 'Jan', 'Kowal', 203433, 'KITCHEN_HELP');
INSERT INTO employee(id, name, surname, pesel, position) VALUES (8, 'Grzegorz', 'Wiśniewski', 11223, 'WASHER');
INSERT INTO employee(id, name, surname, pesel, position) VALUES (9, 'Patrycja', 'Doe', 203333, 'ROOM_HEAD');
INSERT INTO employee(id, name, surname, pesel, position) VALUES (10, 'Julita', 'Kowalska', 2033124333, 'WAITER');
INSERT INTO employee(id, name, surname, pesel, position) VALUES (11, 'Jacek', 'Kowalski', 202343333, 'WAITER');

INSERT INTO client(id, discount, name, surname) VALUES (1, 10, 'John', 'Smith');
INSERT INTO client(id, discount, name, surname) VALUES (2, 10, 'John', 'Doe');
INSERT INTO client(id, discount, name, surname) VALUES (3, 10, 'Jane', 'Doe');
INSERT INTO client(id, discount, name, surname) VALUES (4, 10, 'Emma', 'Doe');

INSERT INTO bill(id, full_price, tip, id_employee, id_client, data) VALUES (1, 99.99, 1.00, 10, 1,'2020-01-26');
INSERT INTO bill(id, full_price, tip, id_employee, id_client, data) VALUES (2, 80.99, 10.00, 10, 1,'2020-01-20');
INSERT INTO bill(id, full_price, tip, id_employee, id_client, data) VALUES (3, 10.99, 100.00, 10, 2,'2019-01-26');
INSERT INTO bill(id, full_price, tip, id_employee, id_client, data) VALUES (4, 10.99, 0.00, 10, 2,'2020-01-10');
INSERT INTO bill(id, full_price, tip, id_employee, id_client, data) VALUES (5, 0.99, 10000.00, 10, 3,'2020-01-09');

INSERT INTO bill(id, full_price, tip, id_employee, id_client, data) VALUES (6, 0.99, 10000.00, 10, 4,'2020-01-09');
INSERT INTO dishes_bill(ID_DISH, ID_BILL) VALUES (1,1);
INSERT INTO dishes_bill(ID_DISH, ID_BILL) VALUES (2,1);
INSERT INTO dishes_bill(ID_DISH, ID_BILL) VALUES (1,1);

SELECT setval ('public.hibernate_sequence', 11 , true );