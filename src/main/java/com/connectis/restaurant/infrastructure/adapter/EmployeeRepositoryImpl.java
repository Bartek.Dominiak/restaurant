package com.connectis.restaurant.infrastructure.adapter;

import com.connectis.restaurant.domain.model.Employee;
import com.connectis.restaurant.domain.repository.EmployeeRepository;
import com.connectis.restaurant.infrastructure.entity.EmployeeEntity;
import com.connectis.restaurant.infrastructure.entity.Position;
import com.connectis.restaurant.infrastructure.repository.EmployeeEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private final EmployeeEntityRepository employeeEntityRepository;

    @Autowired
    public EmployeeRepositoryImpl(EmployeeEntityRepository employeeEntityRepository) {
        this.employeeEntityRepository = employeeEntityRepository;
    }

    @Override
    public Employee createEmployee(
            Long id,
            String name,
            String surname,
            Long pesel,
            Position position) {
        EmployeeEntity employeeEntity = new EmployeeEntity(
                name,
                surname,
                pesel,
                position
        );
        employeeEntityRepository.save(employeeEntity);
        return toDomain(employeeEntity);
    }

    public Employee toDomain(EmployeeEntity employeeEntity){
        return new Employee(
                employeeEntity.getId(),
                employeeEntity.getName(),
                employeeEntity.getSurname(),
                employeeEntity.getPesel(),
                employeeEntity.getPosition()
        );
    }

    public EmployeeEntity toHibernate(Employee employee){
        return new EmployeeEntity(
                employee.getName(),
                employee.getSurname(),
                employee.getPesel(),
                employee.getPosition()
        );
    }

    @Override
    public void updateEmployee(Long id, String name, String surname, Long pesel, Position position) {
        EmployeeEntity employeeEntity = employeeEntityRepository.findById(id).get();
        employeeEntity.setName(name);
        employeeEntity.setPesel(pesel);
        employeeEntity.setPosition(position);
        employeeEntityRepository.save(employeeEntity);

    }

    @Override
    public void changeName(Long id, String newName) {
        EmployeeEntity employeeEntity = employeeEntityRepository.findById(id).get();
        employeeEntity.setName(newName);
        employeeEntityRepository.save(employeeEntity);
    }

    @Override
    public void changeSurname(Long id, String newSurname) {
        EmployeeEntity employeeEntity = employeeEntityRepository.findById(id).get();
        employeeEntity.setSurname(newSurname);
        employeeEntityRepository.save(employeeEntity);
    }

    @Override
    public void changePesel(Long id, Long newPesel) {
        EmployeeEntity employeeEntity = employeeEntityRepository.findById(id).get();
        employeeEntity.setPesel(newPesel);
        employeeEntityRepository.save(employeeEntity);
    }

    @Override
    public void changePosition(Long id, Position newPosition) {
        EmployeeEntity employeeEntity = employeeEntityRepository.findById(id).get();
        employeeEntity.setPosition(newPosition);
    }

    @Override
    public Optional<Employee> getEmployee(Long id) {
        return employeeEntityRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeEmployee(Long id) {
        employeeEntityRepository.deleteById(id);
    }
}
