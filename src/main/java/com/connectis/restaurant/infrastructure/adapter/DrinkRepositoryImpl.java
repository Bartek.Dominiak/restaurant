package com.connectis.restaurant.infrastructure.adapter;

import com.connectis.restaurant.domain.model.Drinks;
import com.connectis.restaurant.domain.repository.DrinkRepository;
import com.connectis.restaurant.infrastructure.entity.DrinksEntity;
import com.connectis.restaurant.infrastructure.repository.DrinksEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class DrinkRepositoryImpl implements DrinkRepository {

    private final DrinksEntityRepository drinksEntityRepository;

    @Autowired
    public DrinkRepositoryImpl(DrinksEntityRepository drinksEntityRepository) {
        this.drinksEntityRepository = drinksEntityRepository;
    }

    public Drinks toDomain(DrinksEntity drinksEntity) {
        return new Drinks(
                drinksEntity.getId(),
                drinksEntity.getName(),
                drinksEntity.getDescription(),
                drinksEntity.getPrice(),
                drinksEntity.getPortion(),
                drinksEntity.getAvailable()
        );
    }

    public DrinksEntity toHibernate(Drinks drinks) {
        return new DrinksEntity(
                drinks.getId(),
                drinks.getName(),
                drinks.getDescription(),
                drinks.getPrice(),
                drinks.getPortion(),
                drinks.isAvailable()
        );
    }

    public List<DrinksEntity> toHibernate(List<Drinks> drinks){
        return drinks.stream().map(this::toHibernate).collect(Collectors.toList());
    }

    @Override
    public void updateDrink(Long id, String name, String description, Double price, Integer portion, Boolean isAvailable) {
        DrinksEntity drinksEntity = drinksEntityRepository.findDrinkById(id).get();
        drinksEntity.setName(name);
        drinksEntity.setDescription(description);
        drinksEntity.setPrice(price);
        drinksEntity.setPortion(portion);
        drinksEntity.setAvailable(isAvailable);
        drinksEntityRepository.save(drinksEntity);
    }

    @Override
    public Drinks createDrink(String name, String description, Double price, Integer portion, Boolean isAvailable) {
        DrinksEntity drinksEntity = new DrinksEntity(
                null,
                name,
                description,
                price,
                portion,
                isAvailable
        );
        drinksEntityRepository.save(drinksEntity);
        return toDomain(drinksEntity);
    }

    @Override
    public void changeName(Long id, String newName) {
        DrinksEntity drinksEntity = drinksEntityRepository.findDrinkById(id).get();
        drinksEntity.setName(newName);
        drinksEntityRepository.save(drinksEntity);
    }

    @Override
    public void changeDescription(Long id, String newDescription) {
        DrinksEntity drinksEntity = drinksEntityRepository.findDrinkById(id).get();
        drinksEntity.setDescription(newDescription);
        drinksEntityRepository.save(drinksEntity);
    }

    @Override
    public void changePrice(Long id, Double newPrice) {
        DrinksEntity drinksEntity = drinksEntityRepository.findDrinkById(id).get();
        drinksEntity.setPrice(newPrice);
        drinksEntityRepository.save(drinksEntity);
    }

    @Override
    public void changePortion(Long id, Integer newPortion) {
        DrinksEntity drinksEntity = drinksEntityRepository.findDrinkById(id).get();
        drinksEntity.setPortion(newPortion);
        drinksEntityRepository.save(drinksEntity);
    }

    @Override
    public void changeAvailability(Long id, Boolean newAvailability) {
        DrinksEntity drinksEntity = drinksEntityRepository.findDrinkById(id).get();
        drinksEntity.setAvailable(newAvailability);
        drinksEntityRepository.save(drinksEntity);
    }

    @Override
    public Optional<Drinks> getDrinkById(Long id) {
        return drinksEntityRepository.findById(id).map(this::toDomain);
    }

    @Override
    public Optional<Drinks> getDrinkByName(String name) {
        return drinksEntityRepository.findByName(name).map(this::toDomain);
    }

    @Override
    public List<Drinks> getAllDrinks() {
        return drinksEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeDrink(Long id) {
        drinksEntityRepository.deleteById(id);
    }
}
