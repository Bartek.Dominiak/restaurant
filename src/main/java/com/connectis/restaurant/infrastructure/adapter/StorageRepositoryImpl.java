package com.connectis.restaurant.infrastructure.adapter;

import com.connectis.restaurant.domain.model.Dish;
import com.connectis.restaurant.domain.model.Drinks;
import com.connectis.restaurant.domain.model.Storage;
import com.connectis.restaurant.domain.repository.StorageRepository;
import com.connectis.restaurant.infrastructure.entity.StorageEntity;
import com.connectis.restaurant.infrastructure.repository.StorageEntityRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class StorageRepositoryImpl implements StorageRepository {

    private final StorageEntityRepository storageEntityRepository;
    private final DishRepositoryImpl dishes;
    private final DrinkRepositoryImpl drinks;

    public StorageRepositoryImpl(StorageEntityRepository storageEntityRepository, DishRepositoryImpl dishes, DrinkRepositoryImpl drinks) {
        this.storageEntityRepository = storageEntityRepository;
        this.dishes = dishes;
        this.drinks = drinks;
    }

    @Override
    public Storage createStorage(
            Long id,
            String name,
            Double quantity) {
        StorageEntity storageEntity = new StorageEntity(
                name,
                quantity
        );
        storageEntityRepository.save(storageEntity);
        return toDomain(storageEntity);
    }

    @Override
    public Storage updateStorage(Long id, String name, Double quantity) {
        StorageEntity storageEntity = storageEntityRepository.findById(id).get();
        storageEntity.setName(name);
        storageEntity.setQuantity(quantity);
        storageEntityRepository.save(storageEntity);
        return toDomain(storageEntity);
    }

//    public Storage toDomain(StorageEntity storageEntity) {
//        return new Storage(
//                storageEntity.getId(),
//                storageEntity.getName(),
//                storageEntity.getQuantity(),
//                storageEntity.getDishes()
//                        .stream()
//                        .map(dishes::toDomain)
//                        .collect(Collectors.toList()),
//                storageEntity.getDrinks()
//                        .stream()
//                        .map(drinks::toDomain)
//                        .collect(Collectors.toList())
//        );
//    }

    public Storage toDomain(StorageEntity storageEntity) {
        return new Storage(storageEntity.getId(), storageEntity.getName(), storageEntity.getQuantity());
    }

    @Override
    public void changeName(Long id, String newName) {
        StorageEntity storageEntity = storageEntityRepository.findById(id).get();
        storageEntity.setName(newName);
        storageEntityRepository.save(storageEntity);
    }

    @Override
    public void changeQuantity(Long id, Double quantity) {
        StorageEntity storageEntity = storageEntityRepository.findById(id).get();
        storageEntity.setQuantity(quantity);
        storageEntityRepository.save(storageEntity);
    }

    @Override
    public void removeStorage(Long id) {
        storageEntityRepository.deleteById(id);
    }

    @Override
    public Optional<Storage> getStorage(Long id) {
        return storageEntityRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Storage> getAllStorages() {
        return storageEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }
}
