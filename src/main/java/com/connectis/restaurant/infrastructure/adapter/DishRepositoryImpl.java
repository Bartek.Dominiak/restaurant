package com.connectis.restaurant.infrastructure.adapter;

import com.connectis.restaurant.domain.model.Dish;
import com.connectis.restaurant.domain.repository.DishRepository;
import com.connectis.restaurant.infrastructure.entity.DishesEntity;
import com.connectis.restaurant.infrastructure.repository.DishesEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class DishRepositoryImpl implements DishRepository {

    private final DishesEntityRepository dishesEntityRepository;

    @Autowired
    public DishRepositoryImpl(DishesEntityRepository dishesEntityRepository) {
        this.dishesEntityRepository = dishesEntityRepository;
    }

    @Override
    public Dish createDish(String name, String description, Double price, Boolean isAvailable) {
        DishesEntity dishesEntity = new DishesEntity(
                null,
                name,
                description,
                price,
                isAvailable
        );
        dishesEntityRepository.save(dishesEntity);
        return toDomain(dishesEntity);
    }

    @Override
    public Dish updateDish(Long id, String name,
                           String description,
                           Double price,
                           Boolean isAvailable) {
        DishesEntity dishesEntity = dishesEntityRepository.findById(id).get();
        dishesEntity.setName(name);
        dishesEntity.setDescription(description);
        dishesEntity.setPrice(price);
        dishesEntity.setAvailable(isAvailable);
        dishesEntityRepository.save(dishesEntity);
        return toDomain(dishesEntity);

    }

    @Override
    public Optional<Dish> getDishById(Long id) {
        return dishesEntityRepository.findById(id).map(this::toDomain);
    }


    @Override
    public void changeName(Long id, String newName) {
        DishesEntity dish = dishesEntityRepository.findById(id).get();
        dish.setName(newName);
        dishesEntityRepository.save(dish);
    }

    @Override
    public void changeDescription(Long id, String newDescription) {
        DishesEntity dish = dishesEntityRepository.findById(id).get();
        dish.setDescription(newDescription);
        dishesEntityRepository.save(dish);
    }

    @Override
    public void changePrice(Long id, Double newPrice) {
        DishesEntity dish = dishesEntityRepository.findById(id).get();
        dish.setPrice(newPrice);
        dishesEntityRepository.save(dish);
    }

    @Override
    public Optional<Dish> getDishByName(String name) {
        return dishesEntityRepository.findByName(name).map(this::toDomain);
    }

    @Override
    public List<Dish> getAllDishes() {
        List<DishesEntity> all = dishesEntityRepository.findAll();
        return dishesEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeDish(Long id) {
        dishesEntityRepository.deleteById(id);
    }

    public Dish toDomain(DishesEntity dishesEntity) {
        return new Dish(
                dishesEntity.getId(),
                dishesEntity.getName(),
                dishesEntity.getDescription(),
                dishesEntity.getPrice(),
                dishesEntity.getAvailable()
        );
    }

    public DishesEntity toHibernate(Dish dish) {
        return new DishesEntity(
                dish.getId(),
                dish.getName(),
                dish.getDescription(),
                dish.getPrice(),
                dish.isAvailable()
        );
    }

    public List<DishesEntity> toHibernate(List<Dish> dishes){
        return dishes.stream().map(this::toHibernate).collect(Collectors.toList());
    }
}
