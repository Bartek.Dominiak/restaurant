package com.connectis.restaurant.infrastructure.adapter;

import com.connectis.restaurant.domain.model.Client;
import com.connectis.restaurant.domain.repository.ClientRepository;
import com.connectis.restaurant.infrastructure.entity.ClientEntity;
import com.connectis.restaurant.infrastructure.repository.ClientEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ClientRepositoryImpl implements ClientRepository {

    private final ClientEntityRepository clientEntityRepository;

    @Autowired
    public ClientRepositoryImpl(ClientEntityRepository clientEntityRepository) {
        this.clientEntityRepository = clientEntityRepository;
    }

    public Client toDomain(ClientEntity clientEntity){
        return new Client(
                clientEntity.getId(),
                clientEntity.getName(),
                clientEntity.getSurname(),
                clientEntity.getDiscount()
        );
    }

    public ClientEntity toHibernate(Client client){
        return new ClientEntity(
                client.getName(),
                client.getSurname(),
                client.getDiscount()
        );
    }

    @Override
    public Client createClient(
            String name,
            String surname,
            Double discount) {
        ClientEntity clientEntity = new ClientEntity(name, surname, discount);
        clientEntityRepository.save(clientEntity);
        return toDomain(clientEntity);
    }

    @Override
    public void changeNameClient(Long id, String newName) {
        ClientEntity client = clientEntityRepository.findById(id).get();
        client.setName(newName);
        clientEntityRepository.save(client);
    }

    @Override
    public void changeSurnameClient(Long id, String newSurname) {
        ClientEntity clientEntity = clientEntityRepository.findById(id).get();
        clientEntity.setSurname(newSurname);
        clientEntityRepository.save(clientEntity);
    }

    @Override
    public void changeDiscount(Long id, Double newDiscount) {
        ClientEntity clientEntity = clientEntityRepository.findById(id).get();
        clientEntity.setDiscount(newDiscount);
        clientEntityRepository.save(clientEntity);
    }

    @Override
    public void removeClient(Long id) {
        clientEntityRepository.deleteById(id);
    }

    @Override
    public Optional<Client> getClientById(Long id) {
        return clientEntityRepository.findById(id).map(this::toDomain);
    }

    @Override
    public Optional<Client> getClientByName(String name) {
        return clientEntityRepository.findByName(name).map(this::toDomain);
    }

    @Override
    public Optional<Client> getClientBySurname(String surname) {
        return clientEntityRepository.findBySurname(surname).map(this::toDomain);
    }

    @Override
    public List<Client> getAllClients() {
        return clientEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }


}
