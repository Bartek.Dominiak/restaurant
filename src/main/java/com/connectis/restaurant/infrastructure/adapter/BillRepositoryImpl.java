package com.connectis.restaurant.infrastructure.adapter;


import com.connectis.restaurant.domain.model.*;
import com.connectis.restaurant.domain.repository.BillRepository;
import com.connectis.restaurant.infrastructure.entity.*;
import com.connectis.restaurant.infrastructure.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class BillRepositoryImpl implements BillRepository {

    private final BillEntityRepository billEntityRepository;
    private final DishRepositoryImpl dishRepository;
    private final DrinkRepositoryImpl drinkRepository;
    private final EmployeeRepositoryImpl employeeRepository;
    private final ClientRepositoryImpl clientRepository;

    @Autowired
    public BillRepositoryImpl(
            BillEntityRepository billEntityRepository,
            DishRepositoryImpl dishRepository,
            DrinkRepositoryImpl drinkRepository,
            EmployeeRepositoryImpl employeeRepository,
            ClientRepositoryImpl clientRepository) {
        this.billEntityRepository = billEntityRepository;
        this.dishRepository = dishRepository;
        this.drinkRepository = drinkRepository;
        this.employeeRepository = employeeRepository;
        this.clientRepository = clientRepository;
    }

    public Bill toDomain(BillEntity entity) {
        return new Bill(
                entity.getId(),
                entity.getFullPrice(),
                entity.getTip(),
                entity.getData(),
                employeeRepository.toDomain(entity.getEmployee()),
                clientRepository.toDomain(entity.getClient()),
                entity.getDishes()
                        .stream()
                        .map(dishRepository::toDomain)
                        .collect(Collectors.toList()),
                entity.getDrinks()
                        .stream()
                        .map(drinkRepository::toDomain)
                        .collect((Collectors.toList())));
    }


    @Override
    public List<Bill> getAllBills() {
        return billEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }


    @Override
    public Bill createBill(
            Double fullPrice,
            Double tip,
            LocalDateTime date,
            Employee employee,
            Client client) {


        BillEntity billEntity = new BillEntity(
                null,
                fullPrice,
                tip,
                date,
                new EmployeeEntity(employee),
                new ClientEntity(client),
                new ArrayList<DishesEntity>(),
                new ArrayList<DrinksEntity>()
        );
        billEntityRepository.save(billEntity);
        return toDomain(billEntity);
    }

//    @Override
//    public List<Dish> getDishListFromBill(Long billId) {
//        List<DishesEntity> dishList = billEntityRepository.findById(billId).get().getDishes();
//        List<Dish> dishes = dishList.stream().map(dishesEntity -> dishRepository.toDomain()).collect(Collectors.toList());
//        return dishes;
//    }

    @Override
    public Bill addDishToBill(Long billId, Long dishId) {

        BillEntity billEntity = billEntityRepository.findById(billId).get();
        billEntity.getDishes().add((dishRepository.toHibernate(dishRepository.getDishById(dishId).get())));
        billEntityRepository.save(billEntity);
        return toDomain(billEntity);
    }

    @Override
    public Bill addDrinkToBill(Long billId, Long drinkId) {
        BillEntity billEntity = billEntityRepository.findById(billId).get();
        billEntity.getDrinks().add((drinkRepository.toHibernate(drinkRepository.getDrinkById(drinkId).get())));
        billEntityRepository.save(billEntity);
        return toDomain(billEntity);
    }

    @Override
    public Optional<Bill> getBill(Long billId) {
        return billEntityRepository.findById(billId).map((this::toDomain));
    }


    @Override
    public List<Bill> getAllBillsWithClient(Long clientId) {
        Iterable<BillEntity> all = billEntityRepository.findByClient(clientId);

        List<BillEntity> entities = new ArrayList<>();
        for (BillEntity billEntity : all) {
            entities.add(billEntity);
        }
        return entities.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public List<Bill> getAllBillsWithEmployee(Long employeeId) {
        Iterable<BillEntity> all = billEntityRepository.findAll();
        List<BillEntity> entities = new ArrayList<>();
        for (BillEntity billEntity : all) {
            entities.add(billEntity);
        }
        return entities.stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void updateBillPrice(Long billId, Double newPrice) {
            BillEntity billEntity = billEntityRepository.findById(billId).get();
            billEntity.setFullPrice(newPrice);
            billEntityRepository.save(billEntity);
    }

    //TODO
    @Override
    public void updateBill(Long billId, Double fullPrice, Double tip, Employee employee, Client client, Date date, List<Dish> list, List<Drinks> drinks) {

    }

    @Override
    public void removeBill(Long billId) {
        billEntityRepository.deleteById(billId);
    }
}
