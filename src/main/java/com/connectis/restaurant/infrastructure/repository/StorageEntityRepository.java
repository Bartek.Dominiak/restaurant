package com.connectis.restaurant.infrastructure.repository;

import com.connectis.restaurant.infrastructure.entity.StorageEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StorageEntityRepository extends CrudRepository<StorageEntity, Long> {

    List<StorageEntity> findAll();

    Optional<StorageEntity> findById(Long id);
}
