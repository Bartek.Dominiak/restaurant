package com.connectis.restaurant.infrastructure.repository;

import com.connectis.restaurant.infrastructure.entity.BillEntity;
import com.connectis.restaurant.infrastructure.entity.DishesEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface BillEntityRepository extends CrudRepository<BillEntity, Long> {

   /* @Query(value = "SELECT s FROM BillEntity s WHERE function('DATE_TRUNC', 'day', s.startTime) =?1")
    Page<BillEntity> findByStartDate (Date date, Pageable pageable);*/

   List<BillEntity> findByClient (Long clientId);

   List<BillEntity> findAll ();

    List<BillEntity> findAllByEmployee (Long employeeId);

    Optional<BillEntity> findById(Long id);

    Optional<BillEntity> findByClientId (Long id);

    Optional<BillEntity> findByEmployeeId (Long id);


}
