package com.connectis.restaurant.infrastructure.repository;

import com.connectis.restaurant.infrastructure.entity.DishesEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface DishesEntityRepository extends CrudRepository<DishesEntity,Long > {


    Optional<DishesEntity> findById(Long dishId);

    Optional<DishesEntity> findByName(String name);

    List<DishesEntity> findAll();
}
