package com.connectis.restaurant.infrastructure.repository;

import com.connectis.restaurant.infrastructure.entity.EmployeeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface EmployeeEntityRepository extends CrudRepository<EmployeeEntity, Long> {

    Optional<EmployeeEntity> findById (Long id);

    List<EmployeeEntity> findAll();

    Optional<EmployeeEntity> findHeadById(Long employeeId);
}
