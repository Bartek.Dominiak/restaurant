package com.connectis.restaurant.infrastructure.repository;

import com.connectis.restaurant.infrastructure.entity.DrinksEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface DrinksEntityRepository extends PagingAndSortingRepository<DrinksEntity, Long> {

    Optional<DrinksEntity> findDrinkById (Long drinkId);

    List<DrinksEntity> findAll ();

    Optional<DrinksEntity> findByName(String name);
}
