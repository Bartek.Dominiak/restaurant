package com.connectis.restaurant.infrastructure.repository;

import com.connectis.restaurant.infrastructure.entity.ClientEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface    ClientEntityRepository extends CrudRepository<ClientEntity, Long> {

    Optional<ClientEntity> findById(Long clientId);

    List<ClientEntity> findAll();

    Optional<ClientEntity> findByName(String name);

    Optional<ClientEntity> findBySurname(String surname);

}
