package com.connectis.restaurant.infrastructure.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "storage", schema = "public", catalog = "restaurant_database")
public class StorageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private Double quantity;

    @ManyToMany(mappedBy = "storages", cascade = CascadeType.ALL)
    private List<DishesEntity> dishes;

    @ManyToMany(mappedBy = "storages", cascade = CascadeType.ALL)
    private List<DrinksEntity> drinks;

    public StorageEntity(String name, Double quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public StorageEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public List<DishesEntity> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishesEntity> dishes) {
        this.dishes = dishes;
    }

    public List<DrinksEntity> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<DrinksEntity> drinks) {
        this.drinks = drinks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StorageEntity that = (StorageEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(dishes, that.dishes) &&
                Objects.equals(drinks, that.drinks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, quantity, dishes, drinks);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", StorageEntity.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("quantity=" + quantity)
                .add("dishes=" + dishes)
                .add("drinks=" + drinks)
                .toString();
    }
}
