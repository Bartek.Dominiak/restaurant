package com.connectis.restaurant.infrastructure.entity;

import com.connectis.restaurant.domain.model.Bill;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "bill", schema = "public", catalog = "restaurant_database")
public class BillEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Double fullPrice;

    private Double tip;

    private LocalDateTime data;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private EmployeeEntity employee;

    @ManyToOne
    @JoinColumn(name = "id_client")
    private ClientEntity client;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "dishes_bill",
            joinColumns = @JoinColumn(name = "id_dish"),
            inverseJoinColumns = @JoinColumn(name = "id_bill")
    )
    private List<DishesEntity> dishes;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(
            name = "drink_bill",
            joinColumns = @JoinColumn(name = "id_drink"),
            inverseJoinColumns = @JoinColumn(name = "id_bill")
    )
    private List<DrinksEntity> drinks;

    public BillEntity() {
    }

    public BillEntity(Long id,
                      Double fullPrice,
                      Double tip,
                      LocalDateTime data,
                      EmployeeEntity employee,
                      ClientEntity client,
                      List<DishesEntity> dishes,
                      List<DrinksEntity> drinks) {
        this.id = id;
        this.fullPrice = fullPrice;
        this.tip = tip;
        this.data = data;
        this.employee = employee;
        this.client = client;
        this.dishes = dishes;
        this.drinks = drinks;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFullPrice() {
        return fullPrice;
    }

    public void setFullPrice(Double fullPrice) {
        this.fullPrice = fullPrice;
    }

    public Double getTip() {
        return tip;
    }

    public void setTip(Double tip) {
        this.tip = tip;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public EmployeeEntity getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeEntity employee) {
        this.employee = employee;
    }

    public ClientEntity getClient() {
        return client;
    }

    public void setClient(ClientEntity client) {
        this.client = client;
    }

    public List<DishesEntity> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishesEntity> dishes) {
        this.dishes = dishes;
    }

    public List<DrinksEntity> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<DrinksEntity> drinks) {
        this.drinks = drinks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BillEntity that = (BillEntity) o;
        return id == that.id &&
                Double.compare(that.fullPrice, fullPrice) == 0 &&
                Objects.equals(tip, that.tip) &&
                Objects.equals(data, that.data) &&
                Objects.equals(employee, that.employee) &&
                Objects.equals(client, that.client) &&
                Objects.equals(dishes, that.dishes) &&
                Objects.equals(drinks, that.drinks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullPrice, tip, data, employee, client, dishes, drinks);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BillEntity.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("fullPrice=" + fullPrice)
                .add("tip=" + tip)
                .add("data=" + data)
                .add("employee=" + employee)
                .add("client=" + client)
                .add("dishes=" + dishes)
                .add("drinks=" + drinks)
                .toString();
    }
}
