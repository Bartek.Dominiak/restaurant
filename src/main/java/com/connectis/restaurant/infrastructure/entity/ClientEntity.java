package com.connectis.restaurant.infrastructure.entity;

import com.connectis.restaurant.domain.model.Client;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "client", schema = "public", catalog = "restaurant_database")
public class ClientEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "discount")
    private Double discount;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<BillEntity> bills;

    public ClientEntity() {
    }

    public ClientEntity(String name, String surname, Double discount) {
        this.name = name;
        this.surname = surname;
        this.discount = discount;
    }

    public ClientEntity(Long id, String name, String surname, Double discount) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.discount = discount;
    }

    public ClientEntity(Client client) {
        this.id = client.getId();
        this.name = client.getName();
        this.surname = client.getSurname();
        this.discount = client.getDiscount();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public List<BillEntity> getBills() {
        return bills;
    }

    public void setBills(List<BillEntity> bills) {
        this.bills = bills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientEntity that = (ClientEntity) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(discount, that.discount) &&
                Objects.equals(bills, that.bills);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, discount, bills);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ClientEntity.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .add("discount=" + discount)
                .add("bills=" + bills)
                .toString();
    }
}
