package com.connectis.restaurant.infrastructure.entity;

public enum Position {
    GENERAL_MANAGER,
    CHEF,
    KITCHEN_HELP,
    WASHER,
    ROOM_HEAD,
    WAITER,
    BAR_HEAD,
    BARMAN,
    RUNNER
}
