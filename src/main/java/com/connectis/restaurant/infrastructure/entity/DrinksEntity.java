package com.connectis.restaurant.infrastructure.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "drinks", schema = "public", catalog = "restaurant_database")
public class DrinksEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String description;

    private Double price;

    private Integer portion;

    private Boolean isAvailable;

    @ManyToMany(mappedBy = "drinks", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<BillEntity> bills;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(
            name = "drink_bill",
            joinColumns = @JoinColumn(name = "id_drink"),
            inverseJoinColumns = @JoinColumn(name = "id_bill")
    )
    private List<StorageEntity> storages;

    public DrinksEntity() {
    }

    public DrinksEntity(Long id, String name, String description, Double price, Integer portion, Boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.portion = portion;
        this.isAvailable = isAvailable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getPortion() {
        return portion;
    }

    public void setPortion(Integer portion) {
        this.portion = portion;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public List<BillEntity> getBills() {
        return bills;
    }

    public void setBills(List<BillEntity> bills) {
        this.bills = bills;
    }

    public List<StorageEntity> getStorages() {
        return storages;
    }

    public void setStorages(List<StorageEntity> storages) {
        this.storages = storages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrinksEntity that = (DrinksEntity) o;
        return id == that.id &&
                Double.compare(that.price, price) == 0 &&
                isAvailable == that.isAvailable &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(portion, that.portion) &&
                Objects.equals(bills, that.bills) &&
                Objects.equals(storages, that.storages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, portion, isAvailable, bills, storages);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DrinksEntity.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("price=" + price)
                .add("portion=" + portion)
                .add("isAvailable=" + isAvailable)
                .add("bills=" + bills)
                .add("storages=" + storages)
                .toString();
    }
}