package com.connectis.restaurant.infrastructure.entity;

import com.connectis.restaurant.domain.model.Employee;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "employee", schema = "public", catalog = "restaurant_database")
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String surname;

    private Long pesel;

    @Enumerated(EnumType.STRING)
    private Position position;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<BillEntity> bills;

    @ManyToOne(cascade = CascadeType.ALL)
    private EmployeeEntity head;

    public EmployeeEntity() {
    }

    public EmployeeEntity(String name, String surname, Long pesel, Position position) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.position = position;
    }

    public EmployeeEntity(Employee employee){
        this.id = employee.getId();
        this.name = employee.getName();
        this.surname = employee.getSurname();
        this.pesel = employee.getPesel();
        this.position = employee.getPosition();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getPesel() {
        return pesel;
    }

    public void setPesel(Long pesel) {
        this.pesel = pesel;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public List<BillEntity> getBills() {
        return bills;
    }

    public void setBills(List<BillEntity> bills) {
        this.bills = bills;
    }

    public EmployeeEntity getHead() {
        return head;
    }

    public void setHead(EmployeeEntity head) {
        this.head = head;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeEntity that = (EmployeeEntity) o;
        return id == that.id &&
                pesel == that.pesel &&
                Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(position, that.position) &&
                Objects.equals(bills, that.bills);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, pesel, position, bills, head);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", EmployeeEntity.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .add("pesel=" + pesel)
                .add("position=" + position)
                .add("bills=" + bills)
                .toString();
    }
}
