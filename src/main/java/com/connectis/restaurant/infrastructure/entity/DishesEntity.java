package com.connectis.restaurant.infrastructure.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "dishes", schema = "public", catalog = "restaurant_database")
public class DishesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String description;

    private Double price;

    private Boolean isAvailable;

    @ManyToMany(mappedBy = "dishes", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<BillEntity> bills;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "dish_storage",
            joinColumns = @JoinColumn(name = "id_dish"),
            inverseJoinColumns = @JoinColumn(name = "id_storage")
    )
    private List<StorageEntity> storages;

    public DishesEntity() {
    }

    public DishesEntity(Long id, String name, String description, Double price, Boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.isAvailable = isAvailable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public List<BillEntity> getBills() {
        return bills;
    }

    public void setBills(List<BillEntity> bills) {
        this.bills = bills;
    }

    public List<StorageEntity> getStorages() {
        return storages;
    }

    public void setStorages(List<StorageEntity> storages) {
        this.storages = storages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DishesEntity that = (DishesEntity) o;
        return id == that.id &&
                Double.compare(that.price, price) == 0 &&
                isAvailable == that.isAvailable &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(bills, that.bills) &&
                Objects.equals(storages, that.storages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, isAvailable, bills, storages);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DishesEntity.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("price=" + price)
                .add("isAvailable=" + isAvailable)
                .add("bills=" + bills)
                .add("storages=" + storages)
                .toString();
    }
}
