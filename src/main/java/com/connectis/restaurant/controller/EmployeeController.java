package com.connectis.restaurant.controller;


import com.connectis.restaurant.controller.dto.EmployeeDTO;
import com.connectis.restaurant.controller.exception.NotFoundException;
import com.connectis.restaurant.controller.exception.OKException;
import com.connectis.restaurant.domain.model.Employee;
import com.connectis.restaurant.domain.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping(path = "/{id}")
    public EmployeeDTO getEmployee(@PathVariable("id") Long id) {
        Optional<Employee> optionalEmployee = employeeService.getEmployee(id);
        if (!optionalEmployee.isPresent()) {
            throw new NotFoundException();
        }
        return new EmployeeDTO(optionalEmployee.get());
    }

    @DeleteMapping(path = "/{id}")
    public void removeEmployee(@PathVariable("id") Long id) {
        if (!employeeService.getEmployee(id).isPresent()){
            throw new OKException();
        }
        employeeService.removeEmployee(id);
    }

    @PostMapping(path = "/add")
    public Long createEmployee(@RequestBody EmployeeDTO employeeDTO) {
        return employeeService.createEmplyee(
                employeeDTO.getName(),
                employeeDTO.getSurname(),
                employeeDTO.getPesel(),
                employeeDTO.getPosition()
        );
    }

    @PutMapping(path = "/update/{id}")
    public void updateEmployee(@PathVariable ("id") Long id,
                               @RequestBody EmployeeDTO employeeDTO) {
        if (!employeeService.getEmployee(id).isPresent()) {
            throw new NotFoundException();
        }
        employeeService.updateEmployee(id,
                employeeDTO.getName(),
                employeeDTO.getSurname(),
                employeeDTO.getPesel(),
                employeeDTO.getPosition()
        );
    }

    @GetMapping(path = "/all")
    public List<EmployeeDTO> getAllEmployee(){
        List<Employee> employeeList = employeeService.getAllEmployee();
        List<EmployeeDTO> employeeDTOS = new ArrayList<>();
        for (Employee employee : employeeList) {
            employeeDTOS.add(new EmployeeDTO(employee));
        }
        return employeeDTOS;
    }


}
