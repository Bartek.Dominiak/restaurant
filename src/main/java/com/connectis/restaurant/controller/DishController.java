package com.connectis.restaurant.controller;

import com.connectis.restaurant.controller.dto.DishDTO;
import com.connectis.restaurant.controller.exception.NotFoundException;
import com.connectis.restaurant.controller.exception.OKException;
import com.connectis.restaurant.domain.model.Dish;
import com.connectis.restaurant.domain.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/dish")
public class DishController {

    private final DishService dishService;

    @Autowired
    public DishController(DishService dishService) {
        this.dishService = dishService;
    }

    @GetMapping(path = "/{id}")
    public DishDTO getDish(@PathVariable("id") Long id) {
        Optional<Dish> dishOptional = dishService.getDish(id);
        if (!dishOptional.isPresent()) {
            throw new NotFoundException();
        }

        return new DishDTO(dishOptional.get());
    }

    @GetMapping(path = "/all")
    public List<DishDTO> getAllDish() {
        List<Dish> dishList = dishService.getAllDishes();
        List<DishDTO> dishDTOS = new ArrayList<>();
        for (Dish dish : dishList) {
            dishDTOS.add(new DishDTO(dish));
        }
        return dishDTOS;

    }

    @PutMapping(path = "/{id}")
    public void updateDish(@RequestBody DishDTO newDish, @PathVariable Long id) {
        if (!dishService.getDish(id).isPresent()) {
            throw new NotFoundException();
        }
        dishService.updateDish(
                id,
                newDish.getName(),
                newDish.getDescription(),
                newDish.getPrice(),
                newDish.getAvailable()
        );
    }


    @PostMapping(path = "/add")
    public Long addNewDish(@RequestBody DishDTO dishDTO) {
        return dishService.createDish(
                dishDTO.getName(),
                dishDTO.getDescription(),
                dishDTO.getPrice(),
                dishDTO.getAvailable()
        );
    }

    @DeleteMapping(path = "/{id}")
    public void deleteDish(@PathVariable("id") Long id) {
        if (dishService.getDish(id).isPresent()) {
            dishService.removeDish(id);
        } else {
            throw new OKException();
        }
    }


}
