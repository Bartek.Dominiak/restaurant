package com.connectis.restaurant.controller.dto;

import com.connectis.restaurant.domain.model.Drinks;

import java.io.Serializable;

public class DrinksDTO implements Serializable {

    private Long id;
    private String name;
    private String description;
    private Double price;
    private Integer portion;
    private Boolean isAvailable;

    public DrinksDTO() {
    }

    public DrinksDTO(Drinks drinks) {
        this.id = drinks.getId();
        this.name = drinks.getName();
        this.description = drinks.getDescription();
        this.price = drinks.getPrice();
        this.portion = drinks.getPortion();
        this.isAvailable = drinks.isAvailable();
    }

    public Drinks toDomain(){
        return new Drinks(
                id,
                name,
                description,
                price,
                portion,
                isAvailable
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getPortion() {
        return portion;
    }

    public void setPortion(Integer portion) {
        this.portion = portion;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }
}
