package com.connectis.restaurant.controller.dto;
import com.connectis.restaurant.domain.model.Client;

import java.io.Serializable;

public class ClientDTO implements Serializable {

    private Long id;
    private String name;
    private String surname;
    private Double discount;

    public ClientDTO() {
    }

    public ClientDTO(Client client) {
        this.id = client.getId();
        this.name = client.getName();
        this.surname = client.getSurname();
        this.discount = client.getDiscount();
    }

    public Client toDomain(){
        return new Client(
                id,
                name,
                surname,
                discount
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

}
