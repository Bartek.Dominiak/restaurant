package com.connectis.restaurant.controller.dto;


import com.connectis.restaurant.domain.model.Dish;
import com.connectis.restaurant.domain.model.Drinks;
import com.connectis.restaurant.domain.model.Storage;

import java.io.Serializable;
import java.util.List;

public class StorageDTO  implements Serializable {

    private Long id;
    private String name;
    private Double quantity;
    private List<Dish> dishes;
    private List<Drinks> drinks;

    public StorageDTO() {
    }

    public StorageDTO(Storage storage) {
        this.id = storage.getId();
        this.name = storage.getName();
        this.quantity = storage.getQuantity();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public List<Drinks> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drinks> drinks) {
        this.drinks = drinks;
    }
}
