package com.connectis.restaurant.controller.dto;

import com.connectis.restaurant.domain.model.*;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

public class BillDTO implements Serializable {

    private Long id;
    private Double fullPrice;
    private Double tip;
    private LocalDateTime data;
    private EmployeeDTO employee;
    private ClientDTO client;
    private List<Dish> dishes;
    private List<Drinks> drinks;

    public BillDTO() {
    }


    public BillDTO(Bill bill) {
        this.id = bill.getId();
        this.fullPrice = bill.getFullPrice();
        this.tip = bill.getTip();
        this.data = bill.getData();
        this.employee = new EmployeeDTO(bill.getEmployee());
        this.client = new ClientDTO(bill.getClient());
        this.dishes = bill.getDishes();
        this.drinks = bill.getDrinks();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFullPrice() {
        return fullPrice;
    }

    public void setFullPrice(Double fullPrice) {
        this.fullPrice = fullPrice;
    }

    public Double getTip() {
        return tip;
    }

    public void setTip(Double tip) {
        this.tip = tip;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public List<Drinks> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drinks> drinks) {
        this.drinks = drinks;
    }
}
