package com.connectis.restaurant.controller.dto;

import com.connectis.restaurant.domain.model.Bill;
import com.connectis.restaurant.domain.model.Dish;
import com.connectis.restaurant.domain.model.Drinks;
import com.connectis.restaurant.domain.model.Storage;

import java.io.Serializable;
import java.util.List;

public class DishDTO implements Serializable {

    private Long id;
    private String name;
    private String description;
    private Double price;
    private Boolean isAvailable;
    private List<Bill> bills;
    private List<Storage> storages;

    public DishDTO() {
    }

    public DishDTO(Dish dish) {
        this.id = dish.getId();
        this.name = dish.getName();
        this.description = dish.getDescription();
        this.price = dish.getPrice();
        this.isAvailable = dish.isAvailable();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    public List<Bill> getBills() {
        return bills;
    }

    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }

    public List<Storage> getStorages() {
        return storages;
    }

    public void setStorages(List<Storage> storages) {
        this.storages = storages;
    }

    public Dish toDomain() {
        return new Dish(
                id,
                name,
                description,
                price,
                isAvailable
        );
    }

}
