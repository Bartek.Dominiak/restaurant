package com.connectis.restaurant.controller.dto;

import com.connectis.restaurant.domain.model.Employee;
import com.connectis.restaurant.infrastructure.entity.Position;

import java.io.Serializable;

public class EmployeeDTO implements Serializable {

    private Long id;
    private String name;
    private String surname;
    private Long pesel;
    private Position position;
    private Employee head;

    public EmployeeDTO() {
    }

    public EmployeeDTO(Employee employee) {
        this.id = employee.getId();
        this.name = employee.getName();
        this.surname = employee.getSurname();
        this.pesel = employee.getPesel();
        this.position = employee.getPosition();
        this.head = employee.getHead();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getPesel() {
        return pesel;
    }

    public void setPesel(Long pesel) {
        this.pesel = pesel;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Employee getHead() {
        return head;
    }

    public void setHead(Employee head) {
        this.head = head;
    }
}
