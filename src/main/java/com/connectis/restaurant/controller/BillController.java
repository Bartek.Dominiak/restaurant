package com.connectis.restaurant.controller;


import com.connectis.restaurant.controller.dto.BillDTO;
import com.connectis.restaurant.controller.exception.NotFoundException;
import com.connectis.restaurant.domain.model.Bill;
import com.connectis.restaurant.domain.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/bill")
public class BillController {

    private final BillService billService;
    private final ClientService clientService;
    private final EmployeeService employeeService;
    private final DishService dishService;
    private final DrinkService drinkService;

    @Autowired
    public BillController(BillService billService, ClientService clientService, EmployeeService employeeService, DishService dishService, DrinkService drinkService) {
        this.billService = billService;
        this.clientService = clientService;
        this.employeeService = employeeService;
        this.dishService = dishService;
        this.drinkService = drinkService;
    }




    @GetMapping(path = "/{id}")
    public BillDTO getBillById(@PathVariable("id") Long id) {
        if (!billService.getBill(id).isPresent()) {
            throw new NotFoundException();
        }
        Optional<Bill> billOptional = billService.getBill(id);
        return new BillDTO(billOptional.get());
    }

    @GetMapping(path = "/all")
    public List<BillDTO> getAllBills() {
        List<Bill> bills = billService.getAllBills();
        List<BillDTO> billDTOS = new ArrayList<>();
        for (Bill bill : bills) {
            billDTOS.add(new BillDTO(bill));
        }
        return billDTOS;
    }

    @PostMapping(path = "/add")
    public Long addBill(
            @RequestParam(name = "employeeId") Long employeeId,
            @RequestParam(name = "clientId") Long clientId) {
        if (!employeeService.getEmployee(employeeId).isPresent() || !clientService.getClient(clientId).isPresent()) {
            throw new NotFoundException();
        }
        return billService.createBill(employeeId, clientId);
    }

    @PutMapping(path = "/addDish")
    public void addDishToBill(
            @RequestParam(name = "billId") Long billId,
            @RequestParam(name = "dishId") Long dishId) {
        if (!billService.getBill(billId).isPresent() || !dishService.getDish(dishId).isPresent()){
            throw new NotFoundException();
        }
        billService.addDishToBill(billId, dishId);

    }

    @PutMapping(path = "addDrink")
    public void addDrinkToBill(
            @RequestParam(name = "billId") Long billId,
            @RequestParam(name = "drinkId") Long drinkId) {
        if (!billService.getBill(billId).isPresent() || !drinkService.getDrink(drinkId).isPresent()){
            throw new NotFoundException();
        }
        billService.addDrinkToBill(billId, drinkId);
    }
}
