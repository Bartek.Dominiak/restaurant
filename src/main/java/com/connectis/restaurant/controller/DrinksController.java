package com.connectis.restaurant.controller;

import com.connectis.restaurant.controller.dto.DrinksDTO;
import com.connectis.restaurant.controller.exception.NotFoundException;
import com.connectis.restaurant.domain.model.Drinks;
import com.connectis.restaurant.domain.service.DrinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/drinks")
public class DrinksController {

    private final DrinkService drinkService;

    @Autowired
    public DrinksController(DrinkService drinkService) {
        this.drinkService = drinkService;
    }


    @PostMapping(path = "/create")
    public Long newDrink(
            @RequestBody DrinksDTO drinksDTO
    ) {
        return drinkService.createDrink(
                drinksDTO.getName(),
                drinksDTO.getDescription(),
                drinksDTO.getPrice(),
                drinksDTO.getPortion(),
                drinksDTO.getAvailable()
        );
    }

    @PutMapping(path = "/update/{id}")
    public void updateDrink(
            @PathVariable("id") Long id,
            @RequestBody DrinksDTO drinksDTO
    ) {

        if (!drinkService.getDrink(id).isPresent()) {
            throw new NotFoundException();
        }
        drinkService.updateDrink(
                id,
                drinksDTO.getName(),
                drinksDTO.getDescription(),
                drinksDTO.getPrice(),
                drinksDTO.getPortion(),
                drinksDTO.getAvailable()
        );
    }

    @GetMapping(path = "/{id}")
    public DrinksDTO getDrinkById(@PathVariable("id") Long id) {
        Optional<Drinks> drinksOptional = drinkService.getDrink(id);
        if (!drinksOptional.isPresent()) {
            throw new NotFoundException();
        }
        return new DrinksDTO(drinksOptional.get());
    }

    @DeleteMapping(path = "/{id}")
    public void removeClient(@PathVariable("id") Long id) {
        drinkService.removeDrink(id);
    }

    @GetMapping(path = "/all")
    public List<DrinksDTO> getAllDrinks() {
        List<Drinks> drinks = drinkService.getAllDrinks();
        List<DrinksDTO> drinksDTOS = new ArrayList<>();
        for (Drinks drink : drinks) {
            drinksDTOS.add(new DrinksDTO(drink));
        }
        return drinksDTOS;
    }
}
