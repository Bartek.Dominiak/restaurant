package com.connectis.restaurant.controller;


import com.connectis.restaurant.controller.dto.EmployeeDTO;
import com.connectis.restaurant.controller.dto.StorageDTO;
import com.connectis.restaurant.controller.exception.NotFoundException;
import com.connectis.restaurant.controller.exception.OKException;
import com.connectis.restaurant.domain.model.Storage;
import com.connectis.restaurant.domain.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/storage")
public class StorageController {

    private final StorageService storageService;

    @Autowired
    public StorageController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping(path = "/add")
    public Long addEmployee(@RequestBody StorageDTO storageDTO) {
        return storageService.createStorage(
                storageDTO.getName(),
                storageDTO.getQuantity()
        );
    }

    @GetMapping(path = "/{id}")
    public StorageDTO getStorage (@PathVariable ("id") Long id) {
        if (!storageService.getStorage(id).isPresent()){
            throw new NotFoundException();
        }
        return new StorageDTO(storageService.getStorage(id).get());
    }

    @DeleteMapping(path = "/{id}")
    public void removeStorage(@PathVariable ("id") Long id) {
        if (!storageService.getStorage(id).isPresent()) {
            throw new OKException();
        }
        storageService.removeStorage(id);
    }

    @PutMapping(path = "/{id}")
    public void updateStorage(@PathVariable ("id") Long id,
                              @RequestBody StorageDTO storageDTO) {
        if (!storageService.getStorage(id).isPresent()) {
            throw new NotFoundException();
        }
        storageService.updateStorage(
                id,
                storageDTO.getName(),
                storageDTO.getQuantity()
        );
    }

    @GetMapping(path = "/all")
    public List<StorageDTO> getAllStorage(){
        List<Storage> storageList = storageService.getAllStorage();
        List<StorageDTO> storageDTOS = new ArrayList<>();
        for (Storage storage : storageList) {
            storageDTOS.add(new StorageDTO(storage));
        }
        return storageDTOS;
    }
}
