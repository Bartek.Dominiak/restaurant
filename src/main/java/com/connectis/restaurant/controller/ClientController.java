package com.connectis.restaurant.controller;


import com.connectis.restaurant.controller.dto.ClientDTO;
import com.connectis.restaurant.controller.exception.NotFoundException;
import com.connectis.restaurant.controller.exception.OKException;
import com.connectis.restaurant.domain.model.Client;
import com.connectis.restaurant.domain.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/client")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping(path = "/{id}")
    public ClientDTO getClientById(@PathVariable("id") Long id) {
        Optional<Client> clientOptional = clientService.getClient(id);
        if (!clientOptional.isPresent()) {
            throw new NotFoundException();
        }
        return new ClientDTO(clientOptional.get());
    }

    @GetMapping(path = "/all")
    public List<ClientDTO> getAllClients() {
        List<Client> clients = clientService.getAllClients();
        List<ClientDTO> clientDTOS = new ArrayList<>();
        for (Client client : clients) {
            clientDTOS.add(new ClientDTO(client));
        }
        return clientDTOS;
    }

    @PostMapping(path = "/add")
    public Long newClient(
            @RequestBody ClientDTO clientDTO) {
        return clientService.createClient(
                clientDTO.getName(),
                clientDTO.getSurname(),
                clientDTO.getDiscount()
        );
    }

    @PutMapping(path = "/update/{id}")
    public void update(
            @PathVariable("id") Long id,
            @RequestBody ClientDTO clientDTO
    ) {
        if (!clientService.getClient(id).isPresent()){
            throw new NotFoundException();
        }
        clientService.updateClient(id, clientDTO.getName(), clientDTO.getSurname(), clientDTO.getDiscount());
    }

    @PutMapping(path = "/changeName/{id}")
    public void changeName(
            @PathVariable("id") Long id,
            @RequestBody String name
    ) {
        if (!clientService.getClient(id).isPresent()){
            throw new NotFoundException();
        }
        clientService.changeClientName(id, name);
    }

    @PutMapping(path = "/changeSurname/{id}")
    public void changeSurname(
            @PathVariable("id") Long id,
            @RequestBody String surname
    ) {
        if (!clientService.getClient(id).isPresent()){
            throw new NotFoundException();
        }
        clientService.changeClientSurname(id, surname);
    }

    @PutMapping(path = "/changeDiscount/{id}")
    public void changeDiscount(
            @PathVariable("id") Long id,
            @RequestBody Double discount
    ) {
        if (!clientService.getClient(id).isPresent()){
            throw new NotFoundException();
        }
        clientService.changeClientDiscount(id, discount);
    }

    @DeleteMapping(path = "/{id}")
    public void removeClient(
            @PathVariable("id") Long id) {
        if (!clientService.getClient(id).isPresent()){
            throw new OKException();
        }
        clientService.removeClient(id);
    }
}



