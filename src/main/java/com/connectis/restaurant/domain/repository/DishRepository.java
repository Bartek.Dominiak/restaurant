package com.connectis.restaurant.domain.repository;

import com.connectis.restaurant.domain.model.Dish;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface DishRepository {

    Dish createDish(
        String name,
        String description,
        Double price,
        Boolean isAvailable
    );

    void changeName(Long id, String newName);

    void changeDescription(Long id, String newDescription);

    void changePrice(Long id, Double newPrice);

    Optional<Dish> getDishById (Long id);

    Optional<Dish> getDishByName (String name);

    List<Dish> getAllDishes();

    void removeDish(Long id);

    Dish updateDish(Long id,
                    String name,
                    String description,
                    Double price,
                    Boolean isAvailable
    );

}
