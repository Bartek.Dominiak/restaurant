package com.connectis.restaurant.domain.repository;

import com.connectis.restaurant.domain.model.Drinks;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface DrinkRepository {

    Drinks createDrink(
            String name,
            String description,
            Double price,
            Integer portion,
            Boolean isAvailable
    );

    void updateDrink(Long id, String name, String description, Double price, Integer portion, Boolean isAvailable);

    void changeName(Long id, String newName);

    void changeDescription(Long id, String newDescription);

    void changePrice(Long id, Double newPrice);

    void changePortion(Long id, Integer newPortion);

    void changeAvailability(Long id, Boolean newAvailability);

    Optional<Drinks> getDrinkById (Long id);

    Optional<Drinks> getDrinkByName (String name);

    List<Drinks> getAllDrinks();

    void removeDrink(Long id);
}
