package com.connectis.restaurant.domain.repository;

import com.connectis.restaurant.domain.model.Bill;
import com.connectis.restaurant.domain.model.Employee;
import com.connectis.restaurant.infrastructure.entity.Position;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {

    Employee createEmployee(
            Long id,
            String name,
            String surname,
            Long pesel,
            Position position
    );

    void changeName(Long id, String newName);

    void changeSurname(Long id, String newSurname);

    void changePesel(Long id, Long newPesel);

    void changePosition(Long id, Position newPosition);

    Optional<Employee> getEmployee(Long id);

    List<Employee> getAllEmployees();

    void removeEmployee(Long id);

    void updateEmployee(Long id,  String name, String surname, Long pesel, Position position);
}
