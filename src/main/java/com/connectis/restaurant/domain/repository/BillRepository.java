package com.connectis.restaurant.domain.repository;

import com.connectis.restaurant.domain.model.*;
import com.connectis.restaurant.infrastructure.entity.BillEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;


import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface BillRepository {
    Bill createBill(Double fullPrice,
                    Double tip,
                    LocalDateTime date,
                    Employee employee,
                    Client client
    );

    Optional<Bill> getBill(Long billId);


/*
    Page<Bill> getAllBillsWithDate(Date date, Pageable pageable);
*/

    Bill addDishToBill(Long billId, Long dishId);

    Bill addDrinkToBill(Long billId, Long drinkId);

    List<Bill> getAllBillsWithClient(Long clientId);

    List<Bill> getAllBillsWithEmployee(Long employeeId);

//    List<Dish> getDishListFromBill(Long billId);

    List<Bill> getAllBills ();

    void updateBillPrice(Long billId, Double newPrice);

    void updateBill(Long billId, Double fullPrice, Double tip, Employee employee, Client client, Date date, List<Dish>dishes, List<Drinks> drinks);

    void removeBill(Long billId);
}
