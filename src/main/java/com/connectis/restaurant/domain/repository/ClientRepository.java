package com.connectis.restaurant.domain.repository;

import com.connectis.restaurant.domain.model.Client;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ClientRepository {

    Client createClient(
            String name,
            String surname,
            Double discount
    );

    void changeNameClient(Long id, String newName);

    void changeSurnameClient(Long id, String newSurname);

    void changeDiscount(Long id, Double newDiscount);

    void removeClient(Long id);

    Optional<Client> getClientById(Long id);

    Optional<Client> getClientByName(String name);

    Optional<Client> getClientBySurname(String surname);

    List<Client> getAllClients();

}
