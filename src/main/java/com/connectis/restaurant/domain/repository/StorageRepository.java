package com.connectis.restaurant.domain.repository;

import com.connectis.restaurant.domain.model.Dish;
import com.connectis.restaurant.domain.model.Drinks;
import com.connectis.restaurant.domain.model.Storage;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface StorageRepository {

    Storage createStorage(
            Long id,
            String name,
            Double quantity
    );

    Storage updateStorage(Long id, String name, Double quantity);

    void changeName(Long id, String newName);

    void changeQuantity(Long id, Double quantity);

    void removeStorage(Long id);

    Optional<Storage> getStorage(Long id);

    List<Storage> getAllStorages();
}
