package com.connectis.restaurant.domain.model;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Storage {

    private Long id;
    private String name;
    private Double quantity;
    private List<Dish> dishes;
    private List<Drinks> drinks;

    public Storage() {
    }

    public Storage(Long id, String name, Double quantity, List<Dish> dishes, List<Drinks> drinks) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.dishes = dishes;
        this.drinks = drinks;
    }

    public Storage(Long id, String name, Double quantity) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public List<Drinks> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drinks> drinks) {
        this.drinks = drinks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Storage storage = (Storage) o;
        return id == storage.id &&
                Objects.equals(name, storage.name) &&
                Objects.equals(quantity, storage.quantity) &&
                Objects.equals(dishes, storage.dishes) &&
                Objects.equals(drinks, storage.drinks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, quantity, dishes, drinks);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Storage.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("quantity=" + quantity)
                .add("dishes=" + dishes)
                .add("drinks=" + drinks)
                .toString();
    }
}
