package com.connectis.restaurant.domain.model;

import com.connectis.restaurant.infrastructure.entity.Position;
import javafx.geometry.Pos;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Employee {

    private Long id;
    private String name;
    private String surname;
    private Long pesel;
    private Position position;
    private Employee head;

    public Employee() {
    }

    public Employee(Long id, String name, String surname, Long pesel, Position position) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.position = position;
    }

    public Employee(Long id, String name, String surname, Long pesel, Position position, Employee head) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.position = position;
        this.head = head;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getPesel() {
        return pesel;
    }

    public void setPesel(Long pesel) {
        this.pesel = pesel;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Employee getHead() {
        return head;
    }

    public void setHead(Employee head) {
        this.head = head;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id &&
                pesel == employee.pesel &&
                Objects.equals(name, employee.name) &&
                Objects.equals(surname, employee.surname) &&
                Objects.equals(position, employee.position) &&
                Objects.equals(head, employee.head);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, pesel, position, head);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Employee.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("surname='" + surname + "'")
                .add("pesel=" + pesel)
                .add("position=" + position)
                .add("head=" + head)
                .toString();
    }
}
