package com.connectis.restaurant.domain.model;

import com.connectis.restaurant.infrastructure.entity.DishesEntity;
import com.connectis.restaurant.infrastructure.entity.DrinksEntity;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Bill {

    private Long id;
    private Double fullPrice;
    private Double tip;
    private LocalDateTime data;
    private Employee employee;
    private Client client;
    private List<Dish> dishes;
    private List<Drinks> drinks;

    public Bill() {
    }

    public Bill(Long id, Double fullPrice, Double tip, LocalDateTime data, Employee employee, Client client, List<Dish> dishes, List<Drinks> drinks) {
        this.id = id;
        this.fullPrice = fullPrice;
        this.tip = tip;
        this.data = data;
        this.employee = employee;
        this.client = client;
        this.dishes = dishes;
        this.drinks = drinks;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFullPrice() {
        return fullPrice;
    }

    public void setFullPrice(Double fullPrice) {
        this.fullPrice = fullPrice;
    }

    public Double getTip() {
        return tip;
    }

    public void setTip(Double tip) {
        this.tip = tip;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public List<Drinks> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drinks> drinks) {
        this.drinks = drinks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bill bill = (Bill) o;
        return id == bill.id &&
                Double.compare(bill.fullPrice, fullPrice) == 0 &&
                Objects.equals(tip, bill.tip) &&
                Objects.equals(data, bill.data) &&
                Objects.equals(employee, bill.employee) &&
                Objects.equals(client, bill.client) &&
                Objects.equals(dishes, bill.dishes) &&
                Objects.equals(drinks, bill.drinks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullPrice, tip, data, employee, client, dishes, drinks);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Bill.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("fullPrice=" + fullPrice)
                .add("tip=" + tip)
                .add("data=" + data)
                .add("employee=" + employee)
                .add("client=" + client)
                .add("dishes=" + dishes)
                .add("drinks=" + drinks)
                .toString();
    }
}
