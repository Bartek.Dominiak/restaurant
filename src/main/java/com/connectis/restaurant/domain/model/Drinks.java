package com.connectis.restaurant.domain.model;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Drinks {

    private Long id;
    private String name;
    private String description;
    private Double price;
    private Integer portion;
    private Boolean isAvailable;

    public Drinks() {
    }

    public Drinks(Long id, String name, String description, Double price, Integer portion, Boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.portion = portion;
        this.isAvailable = isAvailable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getPortion() {
        return portion;
    }

    public void setPortion(Integer portion) {
        this.portion = portion;
    }

    public Boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Drinks drinks = (Drinks) o;
        return id == drinks.id &&
                Double.compare(drinks.price, price) == 0 &&
                isAvailable == drinks.isAvailable &&
                Objects.equals(name, drinks.name) &&
                Objects.equals(description, drinks.description) &&
                Objects.equals(portion, drinks.portion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, portion, isAvailable);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Drinks.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("price=" + price)
                .add("portion=" + portion)
                .add("isAvailable=" + isAvailable)
                .toString();
    }
}
