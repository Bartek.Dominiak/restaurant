package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.Storage;

import java.util.List;
import java.util.Optional;

public interface StorageService {

    Long createStorage(String name, Double quantity);

    Optional<Storage> getStorage(Long storageId);

    void updateStorage(Long storageId, String name, Double quantity);

    void removeStorage(Long storageId);

    List<Storage> getAllStorage();
}
