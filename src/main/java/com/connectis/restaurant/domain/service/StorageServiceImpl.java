package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.Storage;
import com.connectis.restaurant.domain.repository.StorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StorageServiceImpl implements StorageService {


    private final StorageRepository storageRepository;

    @Autowired
    public StorageServiceImpl(StorageRepository storageRepository) {
        this.storageRepository = storageRepository;
    }

    @Override
    public Long createStorage(String name, Double quantity) {
        Storage storage = storageRepository.createStorage(
                null,
                name,
                quantity
        );
        return storage.getId();
    }

    @Override
    public Optional<Storage> getStorage(Long storageId) {
        return storageRepository.getStorage(storageId);
    }

    @Override
    public void updateStorage(Long storageId, String name, Double quantity) {
        storageRepository.updateStorage(storageId, name, quantity);
    }

    @Override
    public void removeStorage(Long storageId) {
        storageRepository.removeStorage(storageId);
    }

    @Override
    public List<Storage> getAllStorage() {
        return storageRepository.getAllStorages();
    }
}
