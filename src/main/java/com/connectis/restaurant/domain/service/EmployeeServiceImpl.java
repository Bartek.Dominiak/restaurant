package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.Employee;
import com.connectis.restaurant.domain.repository.EmployeeRepository;
import com.connectis.restaurant.infrastructure.entity.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    @Override
    public Long createEmplyee(String name, String surname, Long pesel, Position position) {
        Employee employee = employeeRepository.createEmployee(
                null,
                name,
                surname,
                pesel,
                position
        );
        return employee.getId();
    }

    @Override
    public Optional<Employee> getEmployee(Long employeeId) {
        return employeeRepository.getEmployee(employeeId);
    }

    @Override
    public List<Employee> getAllEmployee() {
        return employeeRepository.getAllEmployees();
    }

    @Override
    public void updateEmployee(Long employeeId, String name, String surname, Long pesel, Position position) {
        employeeRepository.updateEmployee(employeeId, name, surname, pesel, position);
    }

    @Override
    public void removeEmployee(Long employeeId) {
        employeeRepository.removeEmployee(employeeId);
    }
}
