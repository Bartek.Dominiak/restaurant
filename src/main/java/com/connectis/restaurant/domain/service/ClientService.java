package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.Client;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ClientService {

    Long createClient (String name, String surname, Double discount);

    Optional<Client> getClient(Long clientId);

    List<Client> getAllClients();

    void updateClient(Long clientId, String name, String surname, Double discount);

    void changeClientName(Long clientId, String name);

    void changeClientSurname(Long clientId, String surname);

    void changeClientDiscount(Long clientId, Double discount);

    void removeClient(Long clientId);
}
