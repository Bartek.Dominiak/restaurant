package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.Dish;
import com.connectis.restaurant.domain.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DishServiceImpl implements DishService {

    private final DishRepository dishRepository;

    @Autowired
    public DishServiceImpl(DishRepository dishRepository) {
        this.dishRepository = dishRepository;
    }

    @Override
    public Long createDish(String name, String description, Double price, Boolean isAvailable) {
        Dish dish = dishRepository.createDish(name, description, price, isAvailable);
        return dish.getId();
    }

    @Override
    public Optional<Dish> getDish(Long dishId) {
        return dishRepository.getDishById(dishId);
    }

    @Override
    public List<Dish> getAllDishes() {
        return dishRepository.getAllDishes();
    }

    @Override
    public void updateDish(Long dishId, String name, String description, Double price, Boolean isAvailable) {
        dishRepository.updateDish(dishId, name, description, price, isAvailable);
    }

    @Override
    public void removeDish(Long dishId) {
        dishRepository.removeDish(dishId);
    }
}
