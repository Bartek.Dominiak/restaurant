package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.Drinks;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface DrinkService {

    Long createDrink (String name, String description, Double price, Integer portion, Boolean isAvailable);

    Optional<Drinks> getDrink (Long drinkId);

    List<Drinks> getAllDrinks();

    void updateDrink (Long id, String name, String description, Double price, Integer portion, Boolean isAvailable);

    void removeDrink (Long drinkId);

    void changeName(Long id, String newName);

    void changeDescription(Long id, String newDescription);

    void changePrice(Long id, Double newPrice);

    void changePortion(Long id, Integer newPortion);

    void changeAvailability(Long id, Boolean newAvailability);
}
