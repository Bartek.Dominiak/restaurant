package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.Employee;
import com.connectis.restaurant.infrastructure.entity.Position;
import javafx.geometry.Pos;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    Long createEmplyee(String name, String surname, Long pesel, Position position);

    Optional<Employee> getEmployee (Long employeeId);

    List<Employee> getAllEmployee ();

    void updateEmployee(Long employeeId, String name, String surname, Long pesel, Position position);

    void removeEmployee (Long employeeId);
}
