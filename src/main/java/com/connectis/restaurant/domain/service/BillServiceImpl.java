package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.controller.exception.NotFoundException;
import com.connectis.restaurant.domain.model.*;
import com.connectis.restaurant.domain.repository.*;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BillServiceImpl implements BillService {

    private final BillRepository billRepository;
    private final EmployeeRepository employeeRepository;
    private final ClientRepository clientRepository;
    private final DishRepository dishRepository;
    private final DrinkRepository drinkRepository;

    public BillServiceImpl(BillRepository billRepository, EmployeeRepository employeeRepository, ClientRepository clientRepository, DishRepository dishRepository, DrinkRepository drinkRepository) {
        this.billRepository = billRepository;
        this.employeeRepository = employeeRepository;
        this.clientRepository = clientRepository;
        this.dishRepository = dishRepository;
        this.drinkRepository = drinkRepository;
    }

    @Override
    public Long createBill(Long employeeId, Long clientId) {

        Client client = clientRepository.getClientById(clientId).orElseThrow(NotFoundException::new);
        Employee employee = employeeRepository.getEmployee(employeeId).orElseThrow(NotFoundException::new);

        Bill bill = billRepository.createBill(
                0.0,
                0.0,
                LocalDateTime.now(),
                employee,
                client
        );
        return bill.getId();
    }


    @Override
    public List<Bill> getAllBills() {
        return billRepository.getAllBills();
    }

    @Override
    public void addDishToBill(Long billId, Long dishId) {
        billRepository.addDishToBill(billId, dishId);
        Double oldPrice = billRepository.getBill(billId).get().getFullPrice();
        Double dishPrice = dishRepository.getDishById(dishId).get().getPrice();
        Double newPrice = oldPrice + dishPrice;
        billRepository.updateBillPrice(billId, newPrice);
    }

    @Override
    public void addDrinkToBill(Long billId, Long drinkId) {
        billRepository.addDrinkToBill(billId, drinkId);
        Double oldPrice = billRepository.getBill(billId).get().getFullPrice();
        Double drinkPrice = drinkRepository.getDrinkById(drinkId).get().getPrice();
        Double newPrice = oldPrice + drinkPrice;
        billRepository.updateBillPrice(billId, newPrice);
    }

    @Override
    public Optional<Bill> getBill(Long billId) {
        return billRepository.getBill(billId);
    }

    @Override
    public void updateBill(Long billId, Double fullPrice, Double tip, Employee employee, Client client, Date date, List<Dish> dishes, List<Drinks> drinks) {
        billRepository.updateBill(billId, fullPrice, tip, employee, client, date, dishes, drinks);
    }

    @Override
    public void removeBill(Long billId) {
        billRepository.removeBill(billId);
    }
}
