package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.Client;
import com.connectis.restaurant.domain.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService{

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Long createClient(String name, String surname, Double discount) {
        Client client = clientRepository.createClient(
                name,
                surname,
                discount
        );
        return client.getId();
    }

    @Override
    public Optional<Client> getClient(Long clientId) {
        return clientRepository.getClientById(clientId);
    }


    @Override
    public List<Client> getAllClients() {
        return clientRepository.getAllClients();
    }

    @Override
    public void updateClient(Long clientId, String name, String surname, Double discount) {
        clientRepository.changeNameClient(clientId, name);
        clientRepository.changeSurnameClient(clientId, surname);
        clientRepository.changeDiscount(clientId, discount);
    }

    @Override
    public void changeClientName(Long clientId, String name) {
        clientRepository.getClientById(clientId).get().setName(name);
    }

    @Override
    public void changeClientSurname(Long clientId, String surname) {
        clientRepository.getClientById(clientId).get().setSurname(surname);
    }

    @Override
    public void changeClientDiscount(Long clientId, Double discount) {
        clientRepository.getClientById(clientId).get().setDiscount(discount);
    }

    @Override
    public void removeClient(Long clientId) {
        clientRepository.removeClient(clientId);
    }
}
