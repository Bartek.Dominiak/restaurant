package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.Dish;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface DishService {

    Long createDish(String name, String description, Double price, Boolean isAvailable);

    Optional<Dish> getDish(Long dishId);

    List<Dish> getAllDishes();

    void updateDish(Long dishId, String name, String description, Double price, Boolean isAvailable);

    void removeDish(Long dishId);
}
