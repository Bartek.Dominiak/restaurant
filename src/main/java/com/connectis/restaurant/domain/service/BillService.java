package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface BillService {

    Long createBill(Long employeeId,
                    Long clientId
    );

    Optional<Bill> getBill(Long billId);

    List<Bill> getAllBills();

    void updateBill(Long billId, Double fullPrice, Double tip, Employee employee, Client client, Date date, List<Dish> dishes, List<Drinks> drinks);

    void removeBill(Long billId);

    void addDishToBill(Long billId, Long dishId);

    void addDrinkToBill(Long billId, Long drinkId);

}
