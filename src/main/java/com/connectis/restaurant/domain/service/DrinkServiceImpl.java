package com.connectis.restaurant.domain.service;

import com.connectis.restaurant.domain.model.Drinks;
import com.connectis.restaurant.domain.repository.DrinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DrinkServiceImpl implements DrinkService {

    private final DrinkRepository drinkRepository;

    @Autowired
    public DrinkServiceImpl(DrinkRepository drinkRepository) {
        this.drinkRepository = drinkRepository;
    }

    @Override
    public Long createDrink(String name, String description, Double price, Integer portion, Boolean isAvailable) {
        Drinks drink = drinkRepository.createDrink(name, description, price, portion, isAvailable);
        return drink.getId();
    }

    @Override
    public void changeName(Long id, String newName) {
        drinkRepository.changeName(id, newName);
    }

    @Override
    public void changeDescription(Long id, String newDescription) {
        drinkRepository.changeDescription(id, newDescription);
    }

    @Override
    public void changePrice(Long id, Double newPrice) {
        drinkRepository.changePrice(id, newPrice);
    }

    @Override
    public void changePortion(Long id, Integer newPortion) {
        drinkRepository.changePortion(id, newPortion);
    }

    @Override
    public void changeAvailability(Long id, Boolean newAvailability) {
        drinkRepository.changeAvailability(id, newAvailability);
    }

    @Override
    public Optional<Drinks> getDrink(Long drinkId) {
        return drinkRepository.getDrinkById(drinkId);
    }

    @Override
    public List<Drinks> getAllDrinks() {
        return drinkRepository.getAllDrinks();
    }

    @Override
    public void updateDrink(Long id, String name, String description, Double price, Integer portion, Boolean isAvailable) {
        drinkRepository.updateDrink(id, name, description, price, portion, isAvailable);
    }

    @Override
    public void removeDrink(Long drinkId) {
        drinkRepository.removeDrink(drinkId);

    }
}
