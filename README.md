Simple Maven project restuarant app with REST, Spring Boot, Data and Hibernate.
App contains working parts responsible for add, remove, update, get Clients, Dishes, Drinks, Employees, Bills and Storage elements.
App requires still a few changes, but most of it works properly.

Example of commands in Postman which can be use:

Drinks:

Get all : http://localhost:8080/drinks/all

Get {id} : http://localhost:8080/drinks/{id}

Delete {id} : http://localhost:8080/drinks/{id}

Post : http://localhost:8080/drinks/create with JASON body

Put update drink {id} http://localhost:8080/drinks/update/{id} with JSON body

JSON body example:
 {
     
    "name": "drink",
    "description": "500 ml of water",
    "price": 0.99,
    "portion": 500,
    "available": false

 } 




Employee:

Get all: http://localhost:8080/employee/all

Get {id} : http://localhost:8080/employee/{id}

Delete employee {id} : http://localhost:8080/employee/{id}

Put update employee {id} : http://localhost:8080/employee/{id} with JSON body

Post add new employee : http://localhost:8080/employee/add with JSON body

JSON body example:

{
    "name": "Jan",
    "surname": "Kowalski",
    "pesel": 124563,
    "position": "GENERAL_MANAGER",
    "head": null
}

Storage:

Get all : http://localhost:8080/storage/all

Get {id} : http://localhost:8080/storage/{id}

Delete storage {id} : http://localhost:8080/storage/{id}

Put update storage element : http://localhost:8080/employee/update/{id} with JSON body

Post add new storage element : http://localhost:8080/employee/add with JSON body

JSON body example:

    {
        "name": "pasta",
        "quantity": 10.0

    }
